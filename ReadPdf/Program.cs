﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;

namespace ReadPdf
{
    class Program
    {
        

        static void Main(string[] args)
        {

            var retVal = pdfText(@"C:\temp\ReadPdf\ReadPdf\07_2019.pdf");
            Console.Write(retVal);

        }

        public static string pdfText(string path)
        {
            PdfReader reader = new PdfReader(path);
            string text = string.Empty;
            for (int page = 1; page <= reader.NumberOfPages; page++)
            {
                text += PdfTextExtractor.GetTextFromPage(reader, page);
            }
            reader.Close();
            return text;
        }
    }
}
